# A Pirates Guidebook

This is a simple guidebook with various resources to learn about piracy, safety guidelines and some more. 

The author is not responsible if readers start World War III or destroy the fabric of spacetime by reading this.

## What is piracy?

~~Piracy is an act of robbery or criminal violence by ship or boat-borne attackers upon another ship or a coastal area, typically with the goal of stealing cargo and other valuable items or properties.~~

Online copyright piracy is the practice of illegally reproducing and sharing information on the internet. (Shoutout to Wikipedia.)

### Why do people pirate? Is it ethical?

Many people resort to piracy for many reasons. Such as:

- Anti-piracy protections limiting games (such as modding or even causing some games to break).
- Just because we can.
- The product is overly-priced.
- To demo a product and "potentially buy a copy".

More info can be found in this [podcast](https://www.youtube.com/watch?v=N8VQw6901Rw).

### 

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* WIP

